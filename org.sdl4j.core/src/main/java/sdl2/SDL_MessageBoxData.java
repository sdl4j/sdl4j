package sdl2;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.PointerByReference;
import java.util.Arrays;
import java.util.List;

/**
 * <i>native declaration : src\main\cpp\SDL2\SDL_messagebox.h:45</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class SDL_MessageBoxData extends Structure {
	/**
	 * < ::SDL_MessageBoxFlags<br>
	 * C type : int
	 */
	public int flags;
	/**
	 * < Parent window, can be NULL<br>
	 * C type : SDL_Window*
	 */
	public PointerByReference window;
	/**
	 * < UTF-8 title<br>
	 * C type : const char*
	 */
	public Pointer title;
	/**
	 * < UTF-8 message text<br>
	 * C type : const char*
	 */
	public Pointer message;
	public int numbuttons;
	/** C type : const SDL_MessageBoxButtonData* */
	public sdl2.SDL_MessageBoxButtonData.ByReference buttons;
	/**
	 * < ::SDL_MessageBoxColorScheme, can be NULL to use system settings<br>
	 * C type : const SDL_MessageBoxColorScheme*
	 */
	public sdl2.SDL_MessageBoxColorScheme.ByReference colorScheme;
	public SDL_MessageBoxData() {
		super();
	}
	protected List<? > getFieldOrder() {
		return Arrays.asList("flags", "window", "title", "message", "numbuttons", "buttons", "colorScheme");
	}
	/**
	 * @param flags < ::SDL_MessageBoxFlags<br>
	 * C type : int<br>
	 * @param window < Parent window, can be NULL<br>
	 * C type : SDL_Window*<br>
	 * @param title < UTF-8 title<br>
	 * C type : const char*<br>
	 * @param message < UTF-8 message text<br>
	 * C type : const char*<br>
	 * @param buttons C type : const SDL_MessageBoxButtonData*<br>
	 * @param colorScheme < ::SDL_MessageBoxColorScheme, can be NULL to use system settings<br>
	 * C type : const SDL_MessageBoxColorScheme*
	 */
	public SDL_MessageBoxData(int flags, PointerByReference window, Pointer title, Pointer message, int numbuttons, sdl2.SDL_MessageBoxButtonData.ByReference buttons, sdl2.SDL_MessageBoxColorScheme.ByReference colorScheme) {
		super();
		this.flags = flags;
		this.window = window;
		this.title = title;
		this.message = message;
		this.numbuttons = numbuttons;
		this.buttons = buttons;
		this.colorScheme = colorScheme;
	}
	public SDL_MessageBoxData(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends SDL_MessageBoxData implements Structure.ByReference {
		
	};
	public static class ByValue extends SDL_MessageBoxData implements Structure.ByValue {
		
	};
}
