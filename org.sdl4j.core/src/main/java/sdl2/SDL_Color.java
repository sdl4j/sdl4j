package sdl2;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
/**
 * <i>native declaration : src\main\cpp\SDL2\SDL_pixels.h:100</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class SDL_Color extends Structure {
	/** C type : Uint8 */
	public byte r;
	/** C type : Uint8 */
	public byte g;
	/** C type : Uint8 */
	public byte b;
	/** C type : Uint8 */
	public byte a;
	public SDL_Color() {
		super();
	}
	protected List<? > getFieldOrder() {
		return Arrays.asList("r", "g", "b", "a");
	}
	/**
	 * @param r C type : Uint8<br>
	 * @param g C type : Uint8<br>
	 * @param b C type : Uint8<br>
	 * @param a C type : Uint8
	 */
	public SDL_Color(byte r, byte g, byte b, byte a) {
		super();
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	public SDL_Color(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends SDL_Color implements Structure.ByReference {
		
	};
	public static class ByValue extends SDL_Color implements Structure.ByValue {
		
	};
}
