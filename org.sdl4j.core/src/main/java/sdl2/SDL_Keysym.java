package sdl2;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

/**
 * <i>native declaration : src\main\cpp\SDL2\SDL_keyboard.h:10</i><br>
 */
public class SDL_Keysym extends Structure {
	/**
	 * @see SDL_Scancode<br>
	 *      < SDL physical key code - see ::SDL_Scancode for details<br>
	 *      C type : SDL_Scancode
	 */
	public int scancode;
	/**
	 * < SDL virtual key code - see ::SDL_Keycode for details<br>
	 * C type : SDL_Keycode
	 */
	public int sym;
	/**
	 * < current key modifiers<br>
	 * C type : Uint16
	 */
	public short mod;
	/** C type : int */
	public int unused;

	public SDL_Keysym() {
		super();
	}

	protected List<?> getFieldOrder() {
		return Arrays.asList("scancode", "sym", "mod", "unused");
	}

	/**
	 * @param scancode
	 * 			@see SDL_Scancode<br>
	 *            < SDL physical key code - see ::SDL_Scancode for details<br>
	 *            C type : SDL_Scancode<br>
	 * @param sym
	 *            < SDL virtual key code - see ::SDL_Keycode for details<br>
	 *            C type : SDL_Keycode<br>
	 * @param mod
	 *            < current key modifiers<br>
	 *            C type : Uint16<br>
	 * @param unused
	 *            C type : int
	 */
	public SDL_Keysym(int scancode, int sym, short mod, int unused) {
		super();
		this.scancode = scancode;
		this.sym = sym;
		this.mod = mod;
		this.unused = unused;
	}

	public SDL_Keysym(Pointer peer) {
		super(peer);
	}

	public static class ByReference extends SDL_Keysym implements Structure.ByReference {

	};

	public static class ByValue extends SDL_Keysym implements Structure.ByValue {

	};
}
