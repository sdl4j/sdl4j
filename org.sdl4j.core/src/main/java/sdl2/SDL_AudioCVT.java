package sdl2;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
import sdl2.SDL2Library.SDL_AudioFilter;
/**
 * <i>native declaration : src\main\cpp\SDL2\SDL_audio.h:55</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class SDL_AudioCVT extends Structure {
	/** < Set to 1 if conversion possible */
	public int needed;
	/**
	 * < Source audio format<br>
	 * C type : SDL_AudioFormat
	 */
	public short src_format;
	/**
	 * < Target audio format<br>
	 * C type : SDL_AudioFormat
	 */
	public short dst_format;
	/** < Rate conversion increment */
	public double rate_incr;
	/**
	 * < Buffer to hold entire audio data<br>
	 * C type : Uint8*
	 */
	public Pointer buf;
	/** < Length of original audio buffer */
	public int len;
	/** < Length of converted audio buffer */
	public int len_cvt;
	/** < buffer must be len*len_mult big */
	public int len_mult;
	/** < Given len, final size is len*len_ratio */
	public double len_ratio;
	/**
	 * < Filter list<br>
	 * C type : SDL_AudioFilter[10]
	 */
	public SDL_AudioFilter filters;
	/** < Current audio conversion function */
	public int filter_index;
	public SDL_AudioCVT() {
		super();
	}
	protected List<? > getFieldOrder() {
		return Arrays.asList("needed", "src_format", "dst_format", "rate_incr", "buf", "len", "len_cvt", "len_mult", "len_ratio", "filters", "filter_index");
	}
	public SDL_AudioCVT(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends SDL_AudioCVT implements Structure.ByReference {
		
	};
	public static class ByValue extends SDL_AudioCVT implements Structure.ByValue {
		
	};
}
