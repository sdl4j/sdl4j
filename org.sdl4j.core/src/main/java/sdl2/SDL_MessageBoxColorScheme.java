package sdl2;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
/**
 * <i>native declaration : src\main\cpp\SDL2\SDL_messagebox.h:35</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class SDL_MessageBoxColorScheme extends Structure {
	/** C type : SDL_MessageBoxColor[SDL_MESSAGEBOX_COLOR_MAX] */
	public SDL_MessageBoxColor[] colors = new SDL_MessageBoxColor[(int)sdl2.SDL2Library.SDL_MessageBoxColorType.SDL_MESSAGEBOX_COLOR_MAX];
	public SDL_MessageBoxColorScheme() {
		super();
	}
	protected List<? > getFieldOrder() {
		return Arrays.asList("colors");
	}
	/** @param colors C type : SDL_MessageBoxColor[SDL_MESSAGEBOX_COLOR_MAX] */
	public SDL_MessageBoxColorScheme(SDL_MessageBoxColor colors[]) {
		super();
		if ((colors.length != this.colors.length)) 
			throw new IllegalArgumentException("Wrong array size !");
		this.colors = colors;
	}
	public SDL_MessageBoxColorScheme(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends SDL_MessageBoxColorScheme implements Structure.ByReference {
		
	};
	public static class ByValue extends SDL_MessageBoxColorScheme implements Structure.ByValue {
		
	};
}
