package org.sdl4j.core;

import sdl2.SDL2Library;
import static sdl2.SDL2Library.*;

public class HelloSDL4j {

	public static SDL2Library sdl = SDL2Library.INSTANCE;

	public static void main(String[] args) {

		// open a window
		SDL_Window win = sdl.SDL_CreateWindow("Hello SDL4j!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 512,
				SDL_WindowFlags.SDL_WINDOW_SHOWN);
		if (win == null) {
			System.out.println("SDL_CreateWindow Error: " + sdl.SDL_GetError());
			return;
		}

		// renderer
		SDL_Renderer ren = sdl.SDL_CreateRenderer(win, -1,
				SDL_RendererFlags.SDL_RENDERER_ACCELERATED | SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC);
		if (ren == null) {
			System.out.println("SDL_CreateRenderer Error: " + sdl.SDL_GetError());
			return;
		}

		sdl.SDL_SetRenderDrawColor(ren, 192, 0, 0, 255);
		sdl.SDL_RenderClear(ren);
		sdl.SDL_RenderPresent(ren);

		sdl.SDL_Delay(2000);

		sdl.SDL_Quit();

	}

}
